# fastDFS-java

#### 介绍
分布式文件系统 SpringBoot+FastDFS+jwt+Vue.js

#### 软件架构
``` 
nginx负载均衡代理
vue2
mybatis
druid数据源
MD5加密
后台分角色菜单管理

linux : centos7.9
libfastcommon-1.0.72.tar.gz
libserverframe-1.2.2.tar.gz
fastdfs-6.11.0.tar.gz
openresty/1.25.3.1
fastdfs-nginx-module-1.24.tar.gz
``` 