/*
 Navicat Premium Data Transfer

 Source Server         : mini7
 Source Server Type    : MySQL
 Source Server Version : 50744
 Source Host           : 192.168.229.141:3306
 Source Schema         : fastdfs

 Target Server Type    : MySQL
 Target Server Version : 50744
 File Encoding         : 65001

 Date: 15/02/2024 15:49:49
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '管理员表id',
  `role_id` int(11) NULL DEFAULT 0 COMMENT '角色id',
  `role_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '管理员名',
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '管理员密码',
  `sex` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别(男，女)',
  `age` int(11) NULL DEFAULT NULL COMMENT '年龄',
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '状态(0:未工作 1：工作中)',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_deleted` tinyint(4) NOT NULL DEFAULT 1 COMMENT '删除标记（0:不可用 1:可用）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 39 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES (1, 1, '超级管理员', 'root', '123456', '女', 18, 0, '123456@qq.com', '12345678902', '2021-06-30 12:14:13', '2024-02-14 21:40:26', 1);
INSERT INTO `admin` VALUES (2, 2, '用户', 'admin', '123456', '男', 17, 0, '123456@qq.com', '12345678902', '2021-06-30 12:14:13', '2022-03-13 08:41:45', 1);

-- ----------------------------
-- Table structure for fast_dfs_file
-- ----------------------------
DROP TABLE IF EXISTS `fast_dfs_file`;
CREATE TABLE `fast_dfs_file`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `file_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文件的fileId',
  `file_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文件路径',
  `file_size` bigint(25) NOT NULL COMMENT '文件大小',
  `file_name` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文件名',
  `ext` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文件的扩展名,不包含（.）',
  `file_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文件类型',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文件表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fast_dfs_file
-- ----------------------------
INSERT INTO `fast_dfs_file` VALUES (13, 'group1/M01/00/00/wKjljWXMPm2AbtENAAkcOFs7y6A405.jpg', 'group1/M01/00/00/wKjljWXMPm2AbtENAAkcOFs7y6A405.jpg', 597048, '1.jpg', 'jpg', 'image/jpeg', '2024-02-14 12:15:41', '2024-02-14 12:15:41');
INSERT INTO `fast_dfs_file` VALUES (18, 'group1/M00/00/00/wKjljWXNuOGAHraDAAcJxQ8GFGU914.png', 'group1/M00/00/00/wKjljWXNuOGAHraDAAcJxQ8GFGU914.png', 461253, '55.png', 'png', 'image/png', '2024-02-15 15:10:26', '2024-02-15 15:10:26');
INSERT INTO `fast_dfs_file` VALUES (19, 'group1/M01/00/00/wKjljWXNuUiAYqNcAAVtZBzi-RU844.png', 'group1/M01/00/00/wKjljWXNuUiAYqNcAAVtZBzi-RU844.png', 355684, '22.png', 'png', 'image/png', '2024-02-15 15:12:08', '2024-02-15 15:12:08');

-- ----------------------------
-- Table structure for fast_dfs_file_type
-- ----------------------------
DROP TABLE IF EXISTS `fast_dfs_file_type`;
CREATE TABLE `fast_dfs_file_type`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `file_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文件类型',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文件类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fast_dfs_file_type
-- ----------------------------
INSERT INTO `fast_dfs_file_type` VALUES (1, 'image/jpeg', NULL, NULL);
INSERT INTO `fast_dfs_file_type` VALUES (2, 'image/png', NULL, NULL);
INSERT INTO `fast_dfs_file_type` VALUES (3, 'image/jpg', NULL, NULL);

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '资源ID',
  `menu_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '资源名称',
  `parent_id` int(11) NULL DEFAULT NULL COMMENT '父id',
  `url` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '资源路径',
  `menus` tinyint(4) NULL DEFAULT 1 COMMENT '是否有子菜单(0:是，1:否)',
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '图标',
  `create_time` datetime NOT NULL COMMENT '创建日期',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `is_delete` tinyint(4) NOT NULL DEFAULT 1 COMMENT '删除标记（0:不可用 1:可用）',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 38 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '菜单栏表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (2, '文件管理', 0, '', 1, 'el-icon-folder', '2021-06-27 19:58:39', '2024-02-14 22:20:52', 1);
INSERT INTO `menu` VALUES (3, '我的文件', 2, 'file', 0, 'icon-folder-opened', '2021-06-27 21:36:01', '2024-02-14 22:19:50', 1);
INSERT INTO `menu` VALUES (4, '用户管理', 0, '', 1, 'el-icon-user', '2024-02-14 21:58:54', '2024-02-14 22:00:02', 1);
INSERT INTO `menu` VALUES (5, '用户', 4, 'user', 0, 'el-icon-s-custom', '2021-06-27 21:37:17', '2024-02-14 22:15:37', 1);
INSERT INTO `menu` VALUES (6, '个人信息管理', 0, NULL, 1, 'el-icon-user', '2022-03-04 17:12:46', '2024-02-14 21:59:26', 1);
INSERT INTO `menu` VALUES (7, '我的信息', 6, 'info', 0, 'el-icon-user', '2022-03-04 17:13:23', '2024-02-14 22:00:27', 1);
INSERT INTO `menu` VALUES (8, '修改邮箱', 6, 'email', 0, 'el-icon-user', '2022-03-05 10:53:37', '2024-02-14 22:00:29', 1);
INSERT INTO `menu` VALUES (9, '修改密码', 6, 'password', 0, 'el-icon-user', '2022-03-05 10:54:53', '2024-02-14 22:00:33', 1);
INSERT INTO `menu` VALUES (10, '文件上传', 2, 'upload', 0, 'el-icon-upload', '2024-02-14 22:18:49', '2024-02-14 22:18:49', 1);

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `role_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色表id',
  `role_name` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名\r\n1.医生\r\n2.科室\r\n3.医院管理员\r\n4.超级管理员',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色描述',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `is_delete` tinyint(4) NOT NULL DEFAULT 1 COMMENT '删除标记（0:不可用 1:可用）',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES (1, '超级管理员', '拥有所有权限', '2022-03-04 14:46:19', '2022-03-04 15:30:00', 1);
INSERT INTO `role` VALUES (2, '用户', '用户', '2022-03-04 14:46:19', '2024-02-14 21:53:44', 1);

-- ----------------------------
-- Table structure for role_menu
-- ----------------------------
DROP TABLE IF EXISTS `role_menu`;
CREATE TABLE `role_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色菜单表id',
  `role_id` int(11) NOT NULL COMMENT '角色表id(外键)',
  `menu_id` int(11) NOT NULL COMMENT '菜单表id(外键)',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `is_delete` tinyint(4) NOT NULL DEFAULT 1 COMMENT '删除标记（0:不可用 1:可用）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 137 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of role_menu
-- ----------------------------
INSERT INTO `role_menu` VALUES (1, 2, 10, '2024-02-14 22:20:19', '2024-02-14 22:20:19', 1);
INSERT INTO `role_menu` VALUES (2, 2, 2, '2024-02-14 22:02:28', '2024-02-14 22:14:49', 1);
INSERT INTO `role_menu` VALUES (3, 2, 3, '2024-02-14 22:02:37', '2024-02-14 22:14:49', 1);
INSERT INTO `role_menu` VALUES (4, 2, 6, '2024-02-14 22:03:05', '2024-02-14 22:14:49', 1);
INSERT INTO `role_menu` VALUES (5, 2, 7, '2024-02-14 22:03:15', '2024-02-14 22:14:49', 1);
INSERT INTO `role_menu` VALUES (6, 2, 8, '2024-02-14 22:03:23', '2024-02-14 22:14:49', 1);
INSERT INTO `role_menu` VALUES (7, 2, 9, '2024-02-14 22:03:36', '2024-02-14 22:14:49', 1);
INSERT INTO `role_menu` VALUES (9, 1, 4, '2024-02-14 22:04:10', '2024-02-14 22:14:55', 1);
INSERT INTO `role_menu` VALUES (10, 1, 5, '2024-02-14 22:04:18', '2024-02-14 22:14:55', 1);
INSERT INTO `role_menu` VALUES (11, 1, 6, '2024-02-14 22:04:25', '2024-02-14 22:14:55', 1);
INSERT INTO `role_menu` VALUES (12, 1, 7, '2024-02-14 22:04:33', '2024-02-14 22:14:55', 1);
INSERT INTO `role_menu` VALUES (13, 1, 8, '2024-02-14 22:04:44', '2024-02-14 22:14:55', 1);
INSERT INTO `role_menu` VALUES (14, 1, 9, '2024-02-14 22:04:53', '2024-02-14 22:14:55', 1);

SET FOREIGN_KEY_CHECKS = 1;
