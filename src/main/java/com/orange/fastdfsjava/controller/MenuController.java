package com.orange.fastdfsjava.controller;

import com.orange.fastdfsjava.pojo.Admin;
import com.orange.fastdfsjava.pojo.MenuBean;
import com.orange.fastdfsjava.service.AdminService;
import com.orange.fastdfsjava.service.MenuBeanService;
import com.orange.fastdfsjava.utils.AuthContextHolder;
import com.orange.fastdfsjava.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 菜单栏表 前端控制器
 */
@Slf4j
@RestController
@RequestMapping("/menus")
public class MenuController {

    @Resource
    private AdminService adminService;

    @Resource
    private MenuBeanService menuBeanService;

    /**
     * 根据角色获取不同的菜单栏列表
     * @param request
     * @return
     * @throws Exception
     */
    @GetMapping()
    public R getMenuByRoleId(HttpServletRequest request) throws Exception{
        try {
            int adminId = AuthContextHolder.getAdminId(request);
            Admin admin = adminService.getAdminById(adminId);
            Integer roleId = admin.getRoleId();

            List<MenuBean> menuBeanList = menuBeanService.selectByRoleId(roleId);

            return R.ok().setCode(200).setMessage("查询菜单列表success").data("menuBeanList",menuBeanList);
        }catch (Exception e){
            return R.error().setCode(208).setMessage("请求失败，请进行登录后操作");
        }
    }
}