package com.orange.fastdfsjava.mapper;

import com.orange.fastdfsjava.pojo.Admin;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AdminMapper {
    Admin selectByNamePwd(String name, String password);

    Admin selectById(Integer id);

    int selectCountByNamePwd(String name, String password);
}
