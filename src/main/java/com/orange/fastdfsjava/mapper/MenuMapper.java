package com.orange.fastdfsjava.mapper;

import com.orange.fastdfsjava.pojo.Menu;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MenuMapper {
    Menu selectById(Integer menuId);
}
