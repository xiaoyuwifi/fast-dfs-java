package com.orange.fastdfsjava.mapper;

import com.orange.fastdfsjava.pojo.RoleMenu;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface RoleMenuMapper {
    List<RoleMenu> selectByRoleId(Integer roleId);
}
