package com.orange.fastdfsjava.mapper;

import com.orange.fastdfsjava.pojo.FastDfsFile;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface FastDfsFileMapper {
    void insert(FastDfsFile fastDfsFile);

    Long selectByFileId(String fileId);

    int deleteById(Long id);

    List<FastDfsFile> selectAll();

    int selectCount();

    List<FastDfsFile> findeByPage(int offset, int limit);

    void updateById(FastDfsFile fastDfsFile);

    FastDfsFile selectById(Long id);
}
