package com.orange.fastdfsjava.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface FastDfsFileTypeMapper {
    int select(String fileType);
}
