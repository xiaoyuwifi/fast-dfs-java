package com.orange.fastdfsjava.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 跨域配置文件
 **/
@Configuration
public class GlobalCrosConfig implements WebMvcConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")// 对所有路径应用跨域配置,所有的当前站点的请求地址，都支持跨域访问。
                //是否发送Cookie
                .allowCredentials(true)
                //放行哪些原始域
                .allowedHeaders("*")
                .allowedMethods("POST", "GET", "HEAD", "PUT", "OPTIONS", "DELETE")
                .allowedOriginPatterns("*")// 所有的外部域都可跨域访问。
                // 如果是localhost则很难配置，因为在跨域请求的时候，外部域的解析可能是localhost、127.0.0.1、主机名
                .maxAge(3600);// 超时时长设置为1小时。 时间单位是秒。
    }
}

/*
@Configuration
public class GlobalCrosConfig {
    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")    //添加映射路径，“/**”表示对所有的路径实行全局跨域访问权限的设置
                        //.allowedOrigins("*")    //开放哪些ip、端口、域名的访问权限
                        .allowedOriginPatterns("*")    //开放哪些ip、端口、域名的访问权限
                        .allowCredentials(true)  //是否允许发送Cookie信息
                        .allowedMethods("GET", "POST", "PUT", "DELETE")     //开放哪些Http方法，允许跨域访问
                        .allowedHeaders("*")     //允许HTTP请求中的携带哪些Header信息
                        .exposedHeaders("*");   //暴露哪些头部信息（因为跨域访问默认不能获取全部头部信息）
            }
        };
    }
}*/
