package com.orange.fastdfsjava.utils;

import javax.servlet.http.HttpServletRequest;

/**
 * 获取当前用户信息工具类
 **/
public class AuthContextHolder {

    //获取当前用户id
    public static Integer getAdminId(HttpServletRequest request) {
        //从header获取token
        String token = request.getHeader("token");
        //jwt从token获取userid
        Integer adminId = JwtHelper.getAdminId(token);
        return adminId;
    }

    //获取当前用户名称
    public static String getAdminName(HttpServletRequest request) {
        //从header获取token
        String token = request.getHeader("token");
        //jwt从token获取userid
        String adminName = JwtHelper.getAdminName(token);
        return adminName;
    }

}
