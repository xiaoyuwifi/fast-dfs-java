package com.orange.fastdfsjava.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.crypto.hash.Md5Hash;

/**
 * MD5加密工具类
 **/
@Slf4j
public final class ShiroMD5 {

    /**
     * MD5对密码进行加密算法
     */
    public static String getPwd(String email,String password){
        log.info("密码加盐,原密码为："+password);
        Md5Hash md5 = new Md5Hash(password, email, 1024);
        String newMd5Password = md5.toHex();
        System.out.println("加盐后的密码是:" +newMd5Password);
        log.info("密码加密后,生成的新密码为："+newMd5Password);
        return newMd5Password;
    }
}
