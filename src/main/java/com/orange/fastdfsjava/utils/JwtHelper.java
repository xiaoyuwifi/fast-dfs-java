package com.orange.fastdfsjava.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.util.Date;

/**
 * JWT token 工具类
 **/
@Data
@Component
@ConfigurationProperties(prefix = "jwt")
public class JwtHelper {

    //过期时间
    private static long tokenExpiration;
    //签名秘钥
    private static String tokenSignKey;
    //请求头
    private static String tokenHeader;

    //校验token是否正确
    public static boolean verify(String token,Integer adminId,String adminName) {
        try {
            String id = String.valueOf(adminId);
            Algorithm algorithm = Algorithm.HMAC256(id);
            JWTVerifier verifier = JWT.require(algorithm)
                    .withClaim("adminName", adminName)
                    .withClaim("adminId",adminId)
                    .build();
            DecodedJWT jwt = verifier.verify(token);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 生成签名
     */
    public static String createToken(Integer adminId,String adminName) {
        try {
            // 指定过期时间
            Date date = new Date(System.currentTimeMillis() + tokenExpiration);
            String id = String.valueOf(adminId);
            Algorithm algorithm = Algorithm.HMAC256(id);
            return JWT.create()
                    .withSubject("admin")
                    .withClaim("adminName", adminName)
                    .withClaim("adminId",adminId)
                    .withExpiresAt(date)
                    .sign(algorithm);
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    //根据token字符串得到用户id
    public static Integer getAdminId(String token) {
        if(StringUtils.isBlank(token)) {
            return null;
        }
        try {
            DecodedJWT jwt = JWT.decode(token);
            return jwt.getClaim("adminId").asInt();
        } catch (JWTDecodeException e) {
            return null;
        }
    }

    //根据token字符串得到用户名称
    public static String getAdminName(String token) {
        if(StringUtils.isBlank(token)) {
            return "";
        }
        try {
            DecodedJWT jwt = JWT.decode(token);
            return jwt.getClaim("adminName").asString();
        } catch (JWTDecodeException e) {
            return null;
        }
    }



    public static long getTokenExpiration() {
        return tokenExpiration;
    }

    public static void setTokenExpiration(long tokenExpiration) {
        JwtHelper.tokenExpiration = tokenExpiration;
    }

    public static String getTokenSignKey() {
        return tokenSignKey;
    }

    public static void setTokenSignKey(String tokenSignKey) {
        JwtHelper.tokenSignKey = tokenSignKey;
    }

    public static String getTokenHeader() {
        return tokenHeader;
    }

    public static void setTokenHeader(String tokenHeader) {
        JwtHelper.tokenHeader = tokenHeader;
    }

    public static void main(String[] args) {
        String token = JwtHelper.createToken(1,"小鱼儿");
        System.out.println(token);
        System.out.println(JwtHelper.getAdminId(token));
        System.out.println(JwtHelper.getAdminName(token));
        boolean a = verify(token,1,"小鱼儿");
        System.out.println(a);
    }

}