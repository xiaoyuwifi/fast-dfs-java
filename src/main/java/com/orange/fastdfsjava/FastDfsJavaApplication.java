package com.orange.fastdfsjava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FastDfsJavaApplication {

    public static void main(String[] args) {
        SpringApplication.run(FastDfsJavaApplication.class, args);
    }

}
