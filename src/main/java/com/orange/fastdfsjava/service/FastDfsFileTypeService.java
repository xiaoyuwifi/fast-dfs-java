package com.orange.fastdfsjava.service;

public interface FastDfsFileTypeService {
    int selectByFileType(String fileType);
}
