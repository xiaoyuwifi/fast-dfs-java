package com.orange.fastdfsjava.service;

import com.orange.fastdfsjava.pojo.Admin;

public interface AdminService {
    Admin selectAdminLogin(String name, String password);

    Admin getAdminById(Integer adminId);

    int selectCountAdminLogin(String name, String password);
}
