package com.orange.fastdfsjava.service;

import com.orange.fastdfsjava.pojo.FastDfsFile;
import com.orange.fastdfsjava.pojo.PageBean;

import java.util.List;

public interface FastDfsFileService {
    void save(FastDfsFile fastDfsFile);

    void updateById(FastDfsFile fastDfsFile);

    FastDfsFile selectById(Long id);

    Long selectByFileId(String fileId);

    int deleteFastDfsFileById(Long id);

    List<FastDfsFile> selectAll();

    PageBean<FastDfsFile> findFastDfsFileByPage(int currPageNo, int pageSize);
}
