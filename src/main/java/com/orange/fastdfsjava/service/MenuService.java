package com.orange.fastdfsjava.service;

import com.orange.fastdfsjava.pojo.Menu;

public interface MenuService {
    Menu selectByMenuId(Integer menuId);
}
