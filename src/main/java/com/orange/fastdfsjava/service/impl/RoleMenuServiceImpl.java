package com.orange.fastdfsjava.service.impl;

import com.orange.fastdfsjava.pojo.RoleMenu;
import com.orange.fastdfsjava.mapper.RoleMenuMapper;
import com.orange.fastdfsjava.service.RoleMenuService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 角色菜单中间表
 */
@Service
public class RoleMenuServiceImpl implements RoleMenuService {

    @Resource
    private RoleMenuMapper roleMenuMapper;

    @Override
    public List<RoleMenu> selectByRoleId(Integer roleId) {
        return roleMenuMapper.selectByRoleId(roleId);
    }
}