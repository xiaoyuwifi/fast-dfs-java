package com.orange.fastdfsjava.service.impl;

import com.orange.fastdfsjava.pojo.Admin;
import com.orange.fastdfsjava.mapper.AdminMapper;
import com.orange.fastdfsjava.service.AdminService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 管理员表
 */
@Service
public class AdminServiceImpl implements AdminService {

    @Resource
    private AdminMapper adminMapper;

    @Override
    public Admin selectAdminLogin(String name, String password) {
        return adminMapper.selectByNamePwd(name, password);
    }

    @Override
    public Admin getAdminById(Integer id) {
        return adminMapper.selectById(id);
    }

    @Override
    public int selectCountAdminLogin(String name, String password) {
        return adminMapper.selectCountByNamePwd(name, password);
    }
}