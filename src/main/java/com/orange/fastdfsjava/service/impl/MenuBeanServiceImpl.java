package com.orange.fastdfsjava.service.impl;

import com.orange.fastdfsjava.pojo.Menu;
import com.orange.fastdfsjava.pojo.MenuBean;
import com.orange.fastdfsjava.pojo.RoleMenu;
import com.orange.fastdfsjava.mapper.MenuMapper;
import com.orange.fastdfsjava.mapper.RoleMenuMapper;
import com.orange.fastdfsjava.service.MenuBeanService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class MenuBeanServiceImpl implements MenuBeanService {

    @Resource
    private RoleMenuMapper roleMenuMapper;

    @Resource
    private MenuMapper menuMapper;

    @Override
    public List<MenuBean> selectByRoleId(Integer roleId) {

        List<MenuBean> menuBeans = new ArrayList<>();
        List<RoleMenu> roleMenuList = roleMenuMapper.selectByRoleId(roleId);
        for (RoleMenu roleMenu : roleMenuList){
            Menu menu = menuMapper.selectById(roleMenu.getMenuId());
            if (menu.getParentId()==0){
                MenuBean menuBean = new MenuBean();
                menuBean.setMenuId(menu.getMenuId());
                menuBean.setMenuName(menu.getMenuName());
                menuBean.setIcon(menu.getIcon());
                menuBean.setUrl(menu.getUrl());
                menuBean.setParentId(menu.getParentId());
                menuBean.setMenus(menu.getMenus());
                menuBeans.add(menuBean);
            }
        }

        List<MenuBean> menuBeanList = new ArrayList<>();

        for (MenuBean menuBean : menuBeans){
            MenuBean menuBean1 = menuBean;
            List<Menu> menuList = new ArrayList<>();
            for (RoleMenu roleMenu : roleMenuList){
                Menu menu1 = menuMapper.selectById(roleMenu.getMenuId());
                if (menu1.getParentId().equals(menuBean.getMenuId())){
                    menuList.add(menu1);
                }
            }
            menuBean1.setMenuList(menuList);
            menuBeanList.add(menuBean1);
        }

        return menuBeanList;
    }
}