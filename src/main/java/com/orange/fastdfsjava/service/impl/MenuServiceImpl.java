package com.orange.fastdfsjava.service.impl;

import com.orange.fastdfsjava.pojo.Menu;
import com.orange.fastdfsjava.mapper.MenuMapper;
import com.orange.fastdfsjava.service.MenuService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 菜单栏表
 */
@Service
public class MenuServiceImpl implements MenuService {

    @Resource
    private MenuMapper menuMapper;

    @Override
    public Menu selectByMenuId(Integer menuId) {
        return menuMapper.selectById(menuId);
    }
}