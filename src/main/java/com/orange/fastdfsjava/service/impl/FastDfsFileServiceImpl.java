package com.orange.fastdfsjava.service.impl;

import com.orange.fastdfsjava.mapper.FastDfsFileMapper;
import com.orange.fastdfsjava.pojo.FastDfsFile;
import com.orange.fastdfsjava.pojo.PageBean;
import com.orange.fastdfsjava.service.FastDfsFileService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class FastDfsFileServiceImpl implements FastDfsFileService {

    @Resource
    private FastDfsFileMapper fastDfsFileMapper;

    /**
     * 存储数据
     *
     * @param fastDfsFile
     */
    @Override
    public void save(FastDfsFile fastDfsFile) {
        fastDfsFileMapper.insert(fastDfsFile);
    }

    @Override
    public void updateById(FastDfsFile fastDfsFile) {
        fastDfsFileMapper.updateById(fastDfsFile);
    }

    @Override
    public FastDfsFile selectById(Long id) {
        return fastDfsFileMapper.selectById(id);
    }

    /**
     * 根据fileId查询id
     *
     * @param fileId
     * @return
     */
    @Override
    public Long selectByFileId(String fileId) {
        return fastDfsFileMapper.selectByFileId(fileId);
    }

    /**
     * 根据id删除
     *
     * @param id
     * @return
     */
    @Override
    public int deleteFastDfsFileById(Long id) {
        return fastDfsFileMapper.deleteById(id);
    }

    /**
     * 查询所有
     *
     * @return
     */
    @Override
    public List<FastDfsFile> selectAll() {
        return fastDfsFileMapper.selectAll();
    }

    @Override
    public PageBean<FastDfsFile> findFastDfsFileByPage(int currPageNo, int pageSize) {
        PageBean<FastDfsFile> pageBean = new PageBean<>();

        //封装当前页数
        pageBean.setCurrPageNo(currPageNo);
        //每页显示的条数
        pageBean.setPageSize(pageSize);
        //总记录数
        int totalCount = fastDfsFileMapper.selectCount();
        pageBean.setTotalCount(totalCount);
        //总页数=总条数/每页显示的条数
        Double num = Math.ceil(totalCount / pageSize);
        int totalPage = num.intValue();
        pageBean.setTotalPage(totalPage);

        int offset = (currPageNo - 1) * pageSize;
        int limit = pageBean.getPageSize();
        //封装每页显示的数据
        List<FastDfsFile> list = fastDfsFileMapper.findeByPage(offset, limit);
        pageBean.setLists(list);

        return pageBean;
    }
}