package com.orange.fastdfsjava.service.impl;

import com.orange.fastdfsjava.mapper.FastDfsFileTypeMapper;
import com.orange.fastdfsjava.service.FastDfsFileTypeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class FastDfsFileTypeServiceImpl implements FastDfsFileTypeService {

    @Resource
    private FastDfsFileTypeMapper fastDfsFileTypeMapper;

    @Override
    public int selectByFileType(String fileType) {
        return fastDfsFileTypeMapper.select(fileType);
    }
}
