package com.orange.fastdfsjava.service;

import com.orange.fastdfsjava.pojo.RoleMenu;

import java.util.List;

public interface RoleMenuService {
    List<RoleMenu> selectByRoleId(Integer roleId);
}
