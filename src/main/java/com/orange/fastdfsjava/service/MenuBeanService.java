package com.orange.fastdfsjava.service;

import com.orange.fastdfsjava.pojo.MenuBean;

import java.util.List;

public interface MenuBeanService {

    List<MenuBean> selectByRoleId(Integer roleId);
}
