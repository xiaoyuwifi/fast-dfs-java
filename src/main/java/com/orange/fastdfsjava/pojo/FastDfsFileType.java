package com.orange.fastdfsjava.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

/**
 * 文件类型表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Repository
public class FastDfsFileType {
    private Long id;
    private String fileType;//文件类型
    private LocalDateTime createTime; //创建时间
    private LocalDateTime updateTime; //修改时间
}
