package com.orange.fastdfsjava.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 菜单栏表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Repository
public class Menu implements Serializable {

    private Integer menuId; //资源ID

    private String menuName; //资源名称

    private Integer parentId; //父id

    private String url; //资源路径

    private Integer menus; //是否有子菜单(0:是，1:否)

    private String icon; //图标

    private LocalDateTime createTime; //创建日期

    private LocalDateTime updateTime; //更新时间

    private Integer isDelete; //删除标记（0:不可用 1:可用）
}