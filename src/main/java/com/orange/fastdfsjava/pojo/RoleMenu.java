package com.orange.fastdfsjava.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 角色菜单中间表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Repository
public class RoleMenu implements Serializable {

    private Integer id; //角色菜单表id

    private Integer roleId; //角色表id(外键)

    private Integer menuId; //菜单表id(外键)

    private LocalDateTime createTime; //创建时间

    private LocalDateTime updateTime; //更新时间

    private Integer isDelete; //删除标记（0:不可用 1:可用）
}