package com.orange.fastdfsjava.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 分页查询结果的封装类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageBean<T> {
    //当前页数
    private int currPageNo;

    //每页显示的记录数
    private int pageSize;

    //总记录数
    private int totalCount;

    //总页数=总条数/每页显示的条数
    private int totalPage;

    //每页的显示的数据
    private List<T> lists;
}
