package com.orange.fastdfsjava.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 管理员表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Repository
public class Admin implements Serializable {

    private Integer id; //管理员表id

    private Integer roleId; //角色id

    private String roleName; //角色名

    private String name; //管理员名

    private String password; //管理员密码

    private String sex; //性别(男，女)

    private Integer age; //年龄

    private Integer status; //状态(0:未工作 1：工作中)

    private String email; //邮箱

    private String phone; //手机号

    private LocalDateTime createTime; //创建时间

    private LocalDateTime updateTime; //更新时间

    private Integer isDeleted; //删除标记（0:不可用 1:可用）
}