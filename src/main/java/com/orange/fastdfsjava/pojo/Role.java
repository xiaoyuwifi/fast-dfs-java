package com.orange.fastdfsjava.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 角色表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Repository
public class Role implements Serializable {

    private Integer roleId; //角色表id

    private String roleName; //角色名 1.用户 2.超级管理员

    private String description; //角色描述

    private LocalDateTime createTime; //创建时间

    private LocalDateTime updateTime; //更新时间

    private Integer isDelete; //删除标记（0:不可用 1:可用）
}