package com.orange.fastdfsjava.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

/**
 * 菜单栏表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Repository
public class MenuBean implements Serializable {

    private Integer menuId; //资源ID

    private String menuName; //资源名称

    private Integer parentId; //父id

    private String url; //资源路径

    private Integer menus; //是否有子菜单(0:是，1:否)

    private String icon; //图标

    private List<Menu> menuList;
}