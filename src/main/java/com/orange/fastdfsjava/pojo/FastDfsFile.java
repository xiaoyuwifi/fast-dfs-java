package com.orange.fastdfsjava.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

/**
 * 文件类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Repository
public class FastDfsFile {
    private Long id;
    private String fileId;//文件的fileId
    private String filePath;//文件路径
    private Long fileSize;//文件大小
    private String fileName;//文件名
    private String ext;//文件的扩展名,不包含（.）
    private String fileType;//文件类型
    private LocalDateTime createTime; //创建时间
    private LocalDateTime updateTime; //修改时间
}


