package com.orange.fastdfsjava;

import com.orange.fastdfsjava.pojo.FastDfsFile;
import com.orange.fastdfsjava.pojo.PageBean;
import com.orange.fastdfsjava.service.impl.FastDfsFileServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
public class PageBeanTest {

    @Resource
    private FastDfsFileServiceImpl fastDfsFileService;

    @Test
    public void selectFastDfsFilePageBean() {
        PageBean<FastDfsFile> pageBean = fastDfsFileService.findFastDfsFileByPage(2, 5);
        System.out.println("pageBean = " + pageBean);
    }

    @Test
    public void selectOne() {
        FastDfsFile fastDfsFile = fastDfsFileService.selectById(1L);
        String file_id = fastDfsFile.getFileId();
        String[] splitStr = file_id.split("/");
        String group_name = splitStr[0];
        String remoteFileName = "";
        for (int i = 1; i < splitStr.length; i++) {
            remoteFileName += splitStr[i];
            if (i != splitStr.length - 1) {
                remoteFileName += "/";
            }
        }
        System.out.println("group_name = " + group_name);
        System.out.println("remoteFileName = " + remoteFileName);
    }

}
