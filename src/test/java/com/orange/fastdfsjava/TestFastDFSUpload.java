package com.orange.fastdfsjava;

import org.csource.common.NameValuePair;
import org.csource.fastdfs.*;
import org.junit.jupiter.api.Test;

/**
 * 在此文件中通过fastDSF的client代码访问tracker和storage
 * 通过client的api代码方便 访问 tracker和storage,它们中间走的socket协议
 */
public class TestFastDFSUpload {
    //测试文件上传
    @Test
    public void Upload() {
        //通过fastDSF的client代码访问tracker和storage
        try {
            //加载fastDFS客户端的配置 文件
            ClientGlobal.initByProperties("config/fastdfs-client.properties");
            System.out.println("network_timeout=" + ClientGlobal.g_network_timeout + "ms");
            System.out.println("charset=" + ClientGlobal.g_charset);

            //创建tracker的客户端
            TrackerClient trackerClient = new TrackerClient(ClientGlobal.getG_tracker_group());
            //通过TrackerClient对象获取TrackerServer信息
            //1.29版本以前的方法
            //TrackerServer trackerServer = trackerClient.getConnection();
            //没有trackerClient.getConnection()方法的问题解决
            //1.29版本以后的fastdfs的方法更新
            TrackerServer trackerServer = trackerClient.getTrackerServer();
            StorageServer storageServer = null;

            //每次调用的时候会重新new一个StorageClient()实例，这样每次请求拿到的就是不同的StorageClient，
            //也就意味着每个请求会获取到不同的storageServer，这样就不存在共享变量，也就避免了出现并发的空指针问题
            //最好的解决方案就是每次调用的时候new一个新的实例去使用。在使用FastDFS的时候，尽量不要重用StorageClient
            //参考文章：https://blog.csdn.net/luckykapok918/article/details/80938257
            //定义storage的客户端,建立与Storage服务器的连接
            StorageClient1 storageClient = new StorageClient1(trackerServer, storageServer);

            //文件元信息
            NameValuePair[] metaList = new NameValuePair[1];
            metaList[0] = new NameValuePair("fileName", "52.png");
            String path = "D:\\image\\52.png";

            //执行上传
            String fileInfoes = storageClient.upload_file1(path, "png", metaList);
            System.out.println("upload success. file id is: " + fileInfoes);

            //关闭storage客户端
            storageClient.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
