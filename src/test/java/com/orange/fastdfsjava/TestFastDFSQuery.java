package com.orange.fastdfsjava;

import org.csource.common.NameValuePair;
import org.csource.fastdfs.*;
import org.junit.jupiter.api.Test;

public class TestFastDFSQuery {
    //测试文件查询
    @Test
    public void Query() {
        //通过fastDSF的client代码访问tracker和storage
        try {
            //加载fastDFS客户端的配置 文件
            ClientGlobal.initByProperties("config/fastdfs-client.properties");
            System.out.println("network_timeout=" + ClientGlobal.g_network_timeout + "ms");
            System.out.println("charset=" + ClientGlobal.g_charset);

            //创建tracker的客户端
            TrackerClient trackerClient = new TrackerClient(ClientGlobal.getG_tracker_group());
            //通过TrackerClient对象获取TrackerServer信息
            TrackerServer trackerServer = trackerClient.getTrackerServer();
            StorageServer storageServer = null;

            //定义storage的客户端,建立与Storage服务器的连接
            StorageClient1 storageClient = new StorageClient1(trackerServer, storageServer);

            //查询文件
            //upload success. file id is: group1/M00/00/00/wKjljWXHAauARHa2AAWwwNOt0hY257.png
            String group_name = "group1";
            String remoteFileName = "M00/00/00/wKjljWXHAauARHa2AAWwwNOt0hY257.png";
            String file_id = "group1/M00/00/00/wKjljWXHAauARHa2AAWwwNOt0hY257.png";
            FileInfo fileInfo = storageClient.query_file_info(group_name, remoteFileName);
            System.out.println(fileInfo);
            FileInfo fileInfo1 = storageClient.query_file_info1(file_id);
            System.out.println(fileInfo1);

            //查询文件元信息
            NameValuePair[] metadata = storageClient.get_metadata1(file_id);
            for (NameValuePair pair : metadata) {
                System.out.println("文件元信息 : " + pair.getName() + " ," + pair.getValue());
            }

            //关闭storage客户端
            storageClient.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
